
class App {
    constructor() {

        this.userLevel = +prompt('Please, choose a level 1, 2, 3 (For G-d\'s sake, enter only one digit from 1 to 3)')
        if (this.userLevel === 1) {
            this.level = 750
        }

        if (this.userLevel === 2) {
            this.level = 500
        }

        if (this.userLevel === 3) {
            this.level = 250
        }

        this.randomBlue = this.randomBlue.bind(this);
        this.whack = this.whack.bind(this);

        this.arrRandom = [];
        while(this.arrRandom.length < 99){
            let p = Math.floor(Math.random() * 100) + 1;
            if(this.arrRandom.indexOf(p) === -1) this.arrRandom.push(p);
        }

        console.log(this.arrRandom);


        this.table = document.createElement("table");
        document.body.append(this.table);

        this.row = [];
        this.cell = [];
        this.i = 0;
        this.scoreHuman = 0;
        this.scoreBot = 0;

        for (this.i = 0; this.i < 10; this.i++) {
            this.row[this.i] = document.createElement("tr");
            this.table.append(this.row[this.i]);
            for (let j = 0; j < 10; j++) {
                this.cell[j] = document.createElement("td");
                this.row[this.i].append(this.cell[j]);
            }
        }

        this.tds = document.querySelectorAll('td');
        this.randomBlue();
    }

    randomBlue() {
        if (this.scoreBot === 50) {
            alert('SkyNet supremacy');
            this.table.remove();
            let appNext = new App();
            return
        }

        if (this.scoreHuman === 50) {
            alert('Human beans rule');
            this.table.remove();
            let appNext = new App();
            return
        }

        this.tds[this.arrRandom.slice(-1)[0]].classList.add('blue');
        this.tds[this.arrRandom.slice(-1)[0]].addEventListener('click', this.whack);

        this.scoreBot++;
        console.log('bot ' + this.scoreBot);
        setTimeout(() => {
                this.tds[this.arrRandom.slice(-1)[0]].classList.add('red');
                this.tds[this.arrRandom.slice(-1)[0]].removeEventListener('click', this.whack);
                console.log(this.arrRandom);
                this.arrRandom.pop();
                setTimeout(this.randomBlue, this.level);
            },
            this.level);
    }

    whack(event) {
        event.target.classList.add('green');
        this.scoreBot--;
        this.scoreHuman++;
        console.log('human ' + this.scoreHuman);

    }

}


let app = new App();


